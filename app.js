const csv = require('node-csv').createParser('\t', '', '');
const fs = require('fs');
const stream = require('stream');
const lineReader = require('line-reader');
const Promise = require('bluebird');

const testFolder = '/home/andres/caca';

//UUID 1770292

fs.readdir(testFolder, (err, files) => {
    const filtered = files.filter(file => file.includes("x") && file);

    let allLines = []

    for (let i = 0; i < filtered.length; i++) {
        const file = filtered[i];
        const filePath = `${testFolder}/${file}`

        let headers = [];

        /* if (i === 0) {
            headers = line.split("\t");
            console.log(headers)
        } */


        /*  lineReader.eachLine(filePath, (line) => {
 
             if (line.includes("dagoberto") && line.includes("munoz")) {
                 console.log(line + "\n");
                 allLines.push(line);
 
                 const data = line.split("\t");
 
                 let result = {};
 
                 headers.forEach((k, i) => {
                     result[k] = data[i];
                 });
 
                 console.log(result); 
             }
         }); */

        const eachLine = Promise.promisify(lineReader.eachLine);
        eachLine(filePath, function (line) {
            if (line.toLowerCase().includes("dagoberto") && line.toLowerCase().includes("munoz")) {
                console.log(line);
            }
        }).then(function () {
            //console.log('done');
        }).catch(function (err) {
            //console.error(err);
        });
    }
});